apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "nlx-directory.fullname" . }}-monitor
  labels:
    {{- include "nlx-directory.labels" . | nindent 4 }}
    app.kubernetes.io/component: monitor
spec:
  replicas: 1
  selector:
    matchLabels:
      {{- include "nlx-directory.selectorLabels" . | nindent 6 }}
      app.kubernetes.io/component: monitor
  template:
    metadata:
      labels:
        {{- include "nlx-directory.selectorLabels" . | nindent 8 }}
        app.kubernetes.io/component: monitor
      annotations:
        checksum/secret-postgresql: {{ include (print $.Template.BasePath "/secret-postgresql.yaml") . | sha256sum }}
        checksum/secret-tls: {{ include (print $.Template.BasePath "/secret-tls-monitor.yaml") . | sha256sum }}
    spec:
    {{- with .Values.image.pullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ include "nlx-directory.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
    {{- if .Values.tls.generateCertificate }}
      initContainers:
        - name: generate-certificate
          image: {{ template "nlx-directory.unsafeCA.image" . }}
          imagePullPolicy: {{ .Values.unsafeCA.image.pullPolicy }}
          workingDir: /certs
          command: ["/bin/ash"]
          args:
            - "-exc"
            - |
                /ca/generate-cert.sh "${DOMAIN_NAME}" "${ORGANIZATION_NAME}" "${CFSSL_HOSTNAME}"
                mv nlx_root.pem root.pem
                mv "${DOMAIN_NAME}-key.pem" cert-key.pem
                mv "${DOMAIN_NAME}.pem" cert.pem
          env:
            - name: DOMAIN_NAME
              value: {{ default (print (include "nlx-directory.fullname" .) "-monitor") .Values.unsafeCA.monitorDomain | quote }}
            - name: ORGANIZATION_NAME
              value: {{ required "Unsafe CA organization name is required" (include "nlx-directory.unsafeCA.organizationName" .) | quote }}
            - name: CFSSL_HOSTNAME
              value: {{ required "Unsafe CA CFSSL hostname is required" (include "nlx-directory.unsafeCA.cfsslHostname" .) | quote }}
          volumeMounts:
            - name: certificates
              mountPath: /certs
    {{- end }}
      containers:
        - name: nlx-directory-monitor
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ template "nlx-directory.monitorImage" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: TTL_OFFLINE_SERVICE
              value: {{ .Values.config.monitorOfflineServiceTTL | quote }}
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: {{ template "nlx-directory.postgresql.secret" . }}
                  key: POSTGRES_USER
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ template "nlx-directory.postgresql.secret" . }}
                  key: POSTGRES_PASSWORD
            - name: POSTGRES_DSN
              value: postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@{{ .Values.postgresql.hostname }}/{{ .Values.postgresql.database }}?sslmode=disable&connect_timeout=10
            - name: TLS_NLX_ROOT_CERT
              value: "/certs/root.pem"
            - name: TLS_MONITOR_CERT
              value: "/certs/cert.pem"
            - name: TLS_MONITOR_KEY
              value: "/certs/cert-key.pem"
            - name: LOG_TYPE
              value: {{ .Values.config.logType }}
            - name: LOG_LEVEL
              value: {{ .Values.config.logLevel }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - name: certificates
              mountPath: /certs
      volumes:
        - name: certificates
        {{- if .Values.tls.generateCertificate }}
          emptyDir: {}
        {{- else }}
          secret:
            secretName: {{ default (printf "%s-tls-monitor" (include "nlx-directory.fullname" .)) .Values.tls.monitor.existingSecret }}
        {{- end }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
